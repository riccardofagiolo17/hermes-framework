import {describe, expect, test} from '@jest/globals';
import {ConfigurationFactory, MarkdownProcessor} from '../../src';
import {Literal, Parent} from 'unist';
import ProcessorPlugin from '../../src/processor/ProcessorPlugin';
import {Heading} from 'mdast';

describe('Markdown Processor', () => {

    class TestProcessor<
        C extends Record<string, unknown>
        > extends MarkdownProcessor<C, unknown> {

        getName(): string {
            return 'test';
        }

    }

    test('Tree Extrapolation', async () => {

        const markdown = '# Hello World';

        const processor = new TestProcessor({
            configuration_value: 'value'
        });

        await processor.load(markdown);

        expect(processor.getTree()).toBeTruthy();

        // Check Root Node
        expect(processor.getTree()?.type).toEqual('root');

        // Check Children
        const children = processor.getTree()?.children;

        expect(children).toBeTruthy();

        if(children){
            expect(children.length).toEqual(1);
            expect(children[0].type).toEqual('heading');
            const sub_children = (children[0] as Parent).children;

            expect(sub_children).toBeTruthy();
            if(sub_children){
                expect(sub_children.length).toEqual(1);
                expect(sub_children[0].type).toEqual('text');
                expect((sub_children[0] as Literal).value).toEqual('Hello World');
            }
        }

    });

    test('Plugin', async () => {

        class TestPlugin<
            C extends Record<string, unknown>
            > extends ProcessorPlugin<C>{

            getName(): string {
                return 'test';
            }

            onSetup(configuration: ConfigurationFactory<C>): void {
                // Modify Configuration
                configuration.set('configuration_value', 'plugin');
            }

            onLoad(): void {
            }

        }

        const markdown = '# Hello World';

        const processor = new TestProcessor({
            configuration_value: 'value'
        });

        await processor.load(markdown);
        processor.use(TestPlugin);
        await processor.setup();
        await processor.process();

        const conf = processor.getConfiguration().clone();

        expect(conf.configuration_value).toEqual('plugin');

    });


});
