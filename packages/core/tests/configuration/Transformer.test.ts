import {describe, expect, test} from '@jest/globals';
import {ConfigurationFactory, Transformer} from '../../src';

describe('Transformer', () => {

    const configuration = new ConfigurationFactory({
        configuration_value: 'value'
    });

    class TestTransformer extends Transformer<string, string, Record<string, unknown>> {
        getName(): string {
            return 'test';
        }
        transform(input: string): string {
            return input + ':' + this.configuration.get('configuration_value');
        }
    }

    test('Transformer', () => {

        const tr = new TestTransformer(configuration);

        const result = tr.transform('input');

        expect(tr.getName()).toEqual('test');
        expect(result).toEqual('input:value');

        });

});
