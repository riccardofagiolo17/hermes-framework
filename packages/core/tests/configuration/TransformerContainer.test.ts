import {describe, expect, test} from '@jest/globals';
import {ConfigurationFactory, Transformer, TransformerContainer} from '../../src';

describe('Transformer Container', () => {

    const configuration = new ConfigurationFactory({
        configuration_value: 'value'
    });

    class TestTransformer extends Transformer<string, string, Record<string, unknown>> {
        getName(): string {
            return 'test';
        }
        transform(input: string): string {
            return input + ':' + this.configuration.get('configuration_value');
        }
    }

    test('Transformer List', () => {

        class TestContainer extends TransformerContainer<string, string, Record<string, unknown>>{
            getName(): string {
                return 'test-container';
            }
        }

        const container = new TestContainer(configuration);

        // Pre-Addition Checks
        expect(container.getName()).toEqual('test-container');
        expect(container.getTransformers().length).toEqual(0);
        expect(container.getTransformer('test')).toBeFalsy();

        // Manipulate Container
        container.addTransformer(TestTransformer);
        // Add Twice
        container.addTransformer(TestTransformer);

        // Post-Addition Checks
        expect(container.getTransformers().length).toEqual(1);
        expect(container.getTransformer('test')).toBeTruthy();

        // Check that transformer has access to the passed configuration object
        const string = container.getTransformer('test')?.transform('input');
        expect(string).toEqual('input:value');

        // Manipulate Container
        container.removeTransformer('test');

        // Post-Deletion Checks
        expect(container.getTransformers().length).toEqual(0);
        expect(container.getTransformer('test')).toBeFalsy();

        });

});
