import {describe, expect, test} from '@jest/globals';
import {ConfigurationFactory} from '../../src/index';

describe('Configuration Factory', () => {

    test('Static Clone', () => {

        const original = {
            test: 'hello',
            some_object: {
                deep_array: [1, 2, 3, 4],
                string_array: ['test', 'yo', 'hey']
            }
        };

        const factory = new ConfigurationFactory(original);
        const conf = factory.clone();

        // After cloning the configs must be identical
        expect(conf).toEqual(original);
        // Now we change a string on the original config and the clone must be unaffected
        original.test = 'A new string';
        expect(conf).not.toEqual(original);
        // We revert the changes and try changing the array
        original.test = 'hello';
        original.some_object.string_array.pop();
        expect(conf).not.toEqual(original);
        conf.some_object.string_array.pop();
        expect(conf).toEqual(original);
    });

    test('Merge', () => {

        const factory = new ConfigurationFactory({
            test: 'hello',
            some_object: {
                deep_array: [1, 2, 3, 4],
                string_array: ['test', 'yo', 'hey']
            }
        });
        const conf: any = {
            some_object: {
                merged_object: {
                    merged_property: 'Merged Property'
                }
            }
        };

        factory.merge(conf);

        expect(factory.get('some_object.merged_object')).toBeTruthy();
        expect(factory.get('some_object.merged_object.merged_property')).toBeTruthy();
        expect(factory.get('test')).toBeTruthy();
        const array = factory.get<string[]>('some_object.deep_array');
        expect(array ? array.length > 0 : false).toBeTruthy();

    });

    test('Set & Get', () => {

        const conf: any = {
            some_object: {
                test: 'hello'
            }
        };

        const factory = new ConfigurationFactory(conf);

        expect(factory.get('some_object.test')).toEqual('hello');
        factory.set('some_object.test', 'new_value');
        expect(factory.get('some_object.test')).toEqual('new_value');

    });

});
