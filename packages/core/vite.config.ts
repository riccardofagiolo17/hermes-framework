import {defineConfig} from 'vite';
import * as path from 'path';
import {resolve} from 'path';
import {visualizer} from 'rollup-plugin-visualizer';

const configuration =  defineConfig(() => ({
        build: {
            outDir: 'dist',
            lib: {
                entry: resolve(__dirname, 'src/index.ts'),
                fileName: 'index',
                formats: ['es']
            },
            sourcemap: true
        },
        plugins: [
            visualizer({
                emitFile: true
            })
        ],
        resolve: {
            alias: {
                '@hermes': path.resolve(__dirname, './src/')
            }
        }
    }));

export default configuration;
