#!/bin/bash

WORKDIR=$(dirname "$PWD")
PACKAGES=('core' 'vue')

# Build all the specified packages
for i in "${PACKAGES[@]}"
do
   :
echo "Starting watcher for $i package..."
cd "$WORKDIR" && cd "packages/$i" || exit
(pnpm watch || echo "Watcher for $i package failed.") &
done

