#!/bin/bash

WORKDIR=$(dirname "$PWD")
PACKAGES=('core' 'vue')

# Build all the specified packages
for i in "${PACKAGES[@]}"
do
   :
echo "Building $i package."
cd "$WORKDIR" && cd "packages/$i" || exit
pnpm build > /dev/null &&
echo "Package $i successfully built."
done
